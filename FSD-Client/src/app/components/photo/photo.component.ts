import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss'],
})

export class PhotoComponent implements OnInit, OnDestroy {
  alive = true;
  imageSrc: string;
  constructor(private userService: UserService, private toastrService: ToastrService, private router: Router) {}

  ngOnInit(): void {
  }

  ngOnDestroy(){
    this.alive=false;
  }

  photoChanged(event){
    const reader = new FileReader();
    const image = event.target.files.item(0);
    if(image.type != 'image/png'){
      this.toastrService.error('Wrong file format, it has to be a png image', 'error');
      return;
    }
    
    this.userService.uploadImage(image).subscribe(res => {
      reader.readAsDataURL(image);
      reader.onload = (event: any) => {
        this.imageSrc = event.target.result;
        this.toastrService.success('File uploaded successfully','GJ')
        this.router.navigate(['/history']);
      }
    }, err => {
      console.log(err);
      this.toastrService.error('An error has occured', '')
    })
  }
}
