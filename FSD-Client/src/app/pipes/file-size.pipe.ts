import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileSize',
})
export class FileSizePipe implements PipeTransform {
  transform(value: number): string {
    const val = Number(value/1024).toFixed(2);
    return `${val} kb`;
  }
}
