import { getRepository } from "typeorm";
import { ImageDetails } from "../entity/image-details";
import { NextFunction, Request, response, Response } from "express";
import { MnistData } from "../ml-data/data";
import * as tf from "@tensorflow/tfjs-node";
import { getModel, train } from "../utils/utils";

const IMAGE_WIDTH = 28;
const IMAGE_HEIGHT = 28;
const IMAGE_CHANNELS = 1;
const testDataSize = 1;

export class ImageController {
  private imageRepository = getRepository(ImageDetails);

  async getAllImages(
    request: Request,
    response: Response,
    next: NextFunction
  ): Promise<ImageDetails[] | null> {
    try {
      return await this.imageRepository.find({ order: { id: -1 } });
    } catch (exception) {
      response.sendStatus(500);
    }
  }

  async getImageById(imageId: number): Promise<ImageDetails | null> {
    return await this.imageRepository.findOne({ id: imageId });
  }

  async saveImages(
    request: Request,
    response: Response,
    next: NextFunction
  ): Promise<ImageDetails[]> {
    try {
      return this.imageRepository.save(request.body);
    } catch (exception) {
      response.sendStatus(500);
    }
  }

  async evaluate(request: Request, response: Response, next: NextFunction) {
    try {
      const data = new MnistData();
      await data.load();

      const testData = data.nextTestBatch(testDataSize);

      const testxs = testData.xs.reshape([
        testDataSize,
        IMAGE_WIDTH,
        IMAGE_HEIGHT,
        1,
      ]);

      const model = await tf.loadLayersModel(
        "file://C:/Others/Facultate/Sem2/fsd/FSD-Server/src/ml-data/model.json"
      );
      const labels = testData.labels.argMax(-1);
      const preds = model.predict(testxs);

      testxs.dispose();
      return response.send(`${preds} --- ${labels}`);
    } catch (exception) {
      response.sendStatus(500);
    }
  }

  async train(req: Request, res: Response, next: NextFunction) {
    try {
      const data = new MnistData();
      await data.load();
      const model = getModel();
      await train(model, data).then((results) => {
        model.save(
          "file://C:/Others/Facultate/Sem2/fsd/FSD-Server/src/ml-data/model.json"
        );
        res.send(results);
      });
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  }

  async upload(req: any, res: Response, next: NextFunction) {
    try {
      const file = req.files.file;
      const bufferData = file.data;
      const data = new MnistData();

      await data.loadBuffer(bufferData);

      const model = await tf.loadLayersModel(
        "file://C:/Others/Facultate/Sem2/fsd/FSD-Server/src/ml-data/newTrainModel/model.json"
      );

      const testData = data.nextTestBatch(testDataSize);

      const testxs = testData.xs.reshape([
        testDataSize,
        IMAGE_WIDTH,
        IMAGE_HEIGHT,
        1,
      ]);

      const labels = testData.labels.argMax(-1);
      const preds: any = model.predict(testxs);
      testxs.dispose();

      await preds
        .data()
        .then((data: any[]) => {
          const max = Math.max(...data);
          const imageDetails: ImageDetails = {
            id: undefined,
            name: file.name,
            size: file.size,
            recognitionResult: data.indexOf(max).toString(),
            downloadLink: "coming soon",
          };

          this.imageRepository.save(imageDetails);
          return res.json({ success: true });
        })
        .catch((err) => {
          return response.sendStatus(500);
        });
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  }
}
