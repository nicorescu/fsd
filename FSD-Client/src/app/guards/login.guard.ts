import { Injectable } from '@angular/core';
import {
  CanActivate,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate {
  constructor(private userService: UserService, private router: Router) {}

  buildTree(): Observable<UrlTree> {
    const url = 'photo';
    const tree: UrlTree = this.router.parseUrl(url);
    return of(tree);
  }

  canActivate(): Observable<boolean | UrlTree> {
    if (this.userService.isLoggedOut()) {
      return of(true);
    } else {
      return this.buildTree();
    }
  }
}
