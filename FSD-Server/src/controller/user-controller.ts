import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import * as jwt from "jsonwebtoken";
import { env } from "../env";

export class UserController {
  private userRepository = getRepository(User);

  async getAllUsers(
    request: Request,
    response: Response,
    next: NextFunction
  ): Promise<Error | User[] | null> {
    try {
      return this.userRepository.find();
    } catch (exception) {
      return exception;
    }
  }

  async getUserById(
    request: Request,
    response: Response,
    next: NextFunction
  ): Promise<Error | User | null> {
    if (!request.params.id || typeof request.params.id !== "number")
      return Error("invalid params");
    try {
      return this.userRepository.findOne(request.params.id);
    } catch (exception) {
      return exception;
    }
  }

  async loginByCredentials(
    request: Request,
    response: Response,
    next: NextFunction
  ): Promise<Error | string | null> {
    try {
      const { username, password } = request.body;
      const user = this.userRepository.findOne({
        where: { username: username, password: password },
      });
      user
        .then((user) => {
          console.log(user);
          if (user) {
            const accessToken = jwt.sign({ ...user }, env.ACCESS_TOKEN_SECRET, {
              expiresIn: "24h",
            });
            console.log(accessToken);
            return response.json(accessToken);
          } else {
            return response.sendStatus(401);
          }
        })
        .catch((err) => {
          return err;
        });
    } catch (exception) {
      return exception;
    }
  }

  async saveUser(request: Request, response: Response, next: NextFunction) {
    if (!request.body.username || !request.body.password) {
      return Error("Invalid body");
    }
    try {
      console.log(request.body);
      return this.userRepository.save(request.body);
    } catch (exception) {
      return exception;
    }
  }

  async removeUser(request: Request, response: Response, next: NextFunction) {
    let userToRemove = await this.userRepository.findOne(request.params.id);
    await this.userRepository.remove(userToRemove);
  }
}
