import { Injectable, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class HeaderStateService {
  public isLoggedIn$ = new BehaviorSubject(this.userService.isLoggedIn());

  constructor(private userService: UserService) {}

  public get isLoggedIn() {
    return this.isLoggedIn$.value;
  }

  public set isLoggedIn(v: boolean) {
    this.isLoggedIn$.next(v);
  }
}
