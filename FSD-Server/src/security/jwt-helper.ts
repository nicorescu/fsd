import * as jwt from "jsonwebtoken";
import { env } from "../env";
import { NextFunction, Request, Response } from "express";

export const checkJwt = (req: Request, res: Response, next: NextFunction) : number=> {
  const bearerHeader = req.headers["authorization"];
  let statusCode = 200;
  if (bearerHeader) {
    const bearer = bearerHeader.split(" ");
    const accessToken = bearer[1];
    try {
      jwt.verify(accessToken, env.ACCESS_TOKEN_SECRET, (err, authData) => {
        if (err) {
         statusCode=401;
        }
      });
    } catch (exception) {
      statusCode=500;
    }
  } else {
    statusCode=401;
  }

  return statusCode;
};
