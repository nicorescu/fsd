import { Component, OnDestroy, OnInit } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { ImageDetails } from 'src/app/models/image-details.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-photo-details',
  templateUrl: './photo-details.component.html',
  styleUrls: ['./photo-details.component.scss'],
})
export class PhotoDetailsComponent implements OnInit,OnDestroy {
  items: ImageDetails[];
  alive=true;

  photosColumns: string[] = [
    'name',
    'size',
    'recognitionResult',
    'downloadLink',
  ];

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userService.getImages().pipe(takeWhile(() => this.alive)).subscribe((data)=>{
      this.items = data;
    })
  }

  ngOnDestroy(){
    this.alive=false;
  }
}
