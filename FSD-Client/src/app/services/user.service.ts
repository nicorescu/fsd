import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { RestRequestService } from './rest-request.service';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ImageDetails } from '../models/image-details.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private restService: RestRequestService,
    private router: Router,
    private jwtHelperService: JwtHelperService
  ) {}

  private readonly tokenKey = 'access_token';

  login(user: User): Observable<string> {
    return this.restService.postLogin(user).pipe(
      tap((token) => {
        if (token) {
          this.setToken(token);
          this.router.navigate(['/photo']);
        }
      })
    );
  }

  uploadImage(image: File){
    return this.restService.postImage(image);
  }

  getImages(): Observable<ImageDetails[]>{
    return this.restService.getImages();
  }

  setToken(token: string) {
    localStorage.setItem(this.tokenKey, token);
  }

  getToken(): string {
    return localStorage.getItem(this.tokenKey);
  }

  logout() {
    localStorage.removeItem(this.tokenKey);
  }

  isLoggedIn() {
    const token = this.getToken();
    return token != null && !this.jwtHelperService.isTokenExpired(token);
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }
}
