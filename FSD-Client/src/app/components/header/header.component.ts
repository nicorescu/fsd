import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderStateService } from 'src/app/services/header-state.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, private headerStateService: HeaderStateService) { }

  ngOnInit(): void {
  }

  logout(){
    this.userService.logout();
    this.headerStateService.isLoggedIn = false;
    this.router.navigate(['/login']);
  }

  click() {
    this.router.navigate(['/history']);
    console.log(this.router.url);
  }

}
