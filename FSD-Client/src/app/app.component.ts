import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { HeaderStateService } from './services/header-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  isLoggedIn$: Observable<boolean>;

  constructor(private HeaderStateService: HeaderStateService) {
      this.isLoggedIn$ = this.HeaderStateService.isLoggedIn$;
  }
}
