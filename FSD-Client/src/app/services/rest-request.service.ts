import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { ImageDetails } from '../models/image-details.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class RestRequestService {
  private readonly tokenKey = 'access_token';
  private readonly baseApiUrl = 'http://localhost:3000';

  constructor(
    private httpClient: HttpClient,
    private jwtHelperService: JwtHelperService,
    private router: Router
  ) {}

  public getUser(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${this.baseApiUrl}/users`);
  }

  public postLogin(user: User): Observable<string> {
    return this.httpClient.post<string>(`${this.baseApiUrl}/users/login`, user);
  }

  public postImage(image: File){
    const formData: FormData = new FormData();
    formData.append('file',image);
    return this.httpClient.post(`${this.baseApiUrl}/images/image-upload`,formData);
  }

  public getImages(): Observable<ImageDetails[]>{
    return this.httpClient.get<ImageDetails[]>(`${this.baseApiUrl}/images`);
  }

  getToken(): string {
    return localStorage.getItem(this.tokenKey);
  }

  isLoggedIn() {
    const token = this.getToken();
    return token != null && !this.jwtHelperService.isTokenExpired(token);
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  logout() {
    localStorage.removeItem(this.tokenKey);
  }
}
