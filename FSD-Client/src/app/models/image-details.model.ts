export interface ImageDetails{
    name: string;
    size: number;
    recognitionResult: string;
    downloadLink: string; 
}