import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { HeaderStateService } from 'src/app/services/header-state.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private FormBuilder: FormBuilder,
    private userService: UserService,
    private headerStateService: HeaderStateService,
  ) {}

  ngOnInit(): void {
    this.loginForm = this.FormBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.minLength(8), Validators.required]],
    });
  }

  onSubmit() {
    this.userService.login(this.loginForm.getRawValue() as User).subscribe(
      (token) => {
        if (token) {
          this.headerStateService.isLoggedIn = true;
        }
      },
      (err) => {
        console.log(err);
        this.headerStateService.isLoggedIn = false;
      }
    );
  }

  public get username() {
    return this.loginForm.controls['username'];
  }

  public get password() {
    return this.loginForm.controls['password'];
  }
}
