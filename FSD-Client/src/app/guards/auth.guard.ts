import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router, CanLoad } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { HeaderStateService } from '../services/header-state.service';
import { RestRequestService } from '../services/rest-request.service';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private headerStateService: HeaderStateService,
    private router: Router,
    private jwtHelperService: JwtHelperService
  ) {}

  canActivate(): Observable<boolean | UrlTree> {
    const token = localStorage.getItem('access_token');

    if (token && !this.jwtHelperService.isTokenExpired(token)) {
      return of(true);
    } else {
      this.headerStateService.isLoggedIn=false;
      return of(this.router.createUrlTree(['/login']));
    }
  }
}
