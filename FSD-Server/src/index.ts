import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as jwt from "express-jwt";
import * as cors from "cors";

import { Routes } from "./routes/routes";
import { checkJwt } from "./security/jwt-helper";
import { env } from "./env";
import * as fileUpload from "express-fileupload";

createConnection()
  .then(async (connection) => {
    const app = express();
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use(fileUpload());
    app.use(cors());
    app.use(
      jwt({
        secret: env.ACCESS_TOKEN_SECRET,
        algorithms: ["HS256"],
      }).unless({ path: ["/users/login"] })
    );

    app.use(function (err, req, res, next) {
      if (err.name === "UnauthorizedError") {
        console.log(err);
        res.sendStatus(401);
      }
      res.header("Access-Control-Allow-Origin", "*");
      res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
      );
      next();
    });

    Routes.forEach((route) => {
      (app as any)[route.method](
        route.route,
        (
          req: express.Request,
          res: express.Response,
          next: express.NextFunction
        ) => {
          try {
            if (route.guard) {
              const statusCode = checkJwt(req, res, next);
              if (statusCode === 200) {
                const result = new (route.controller as any)()[route.action](
                  req,
                  res,
                  next
                );
                if (result instanceof Promise) {
                  result.then((result) =>
                    result !== null && result !== undefined
                      ? res.send(result)
                      : undefined
                  );
                } else if (result !== null && result !== undefined) {
                  JSON.stringify(result);
                }
              } else {
                res.sendStatus(statusCode);
              }
            } else {
              const result = new (route.controller as any)()[route.action](
                req,
                res,
                next
              );
              if (result instanceof Promise) {
                result.then((result) =>
                  result !== null && result !== undefined
                    ? res.send(result)
                    : undefined
                );
              } else if (result !== null && result !== undefined) {
                JSON.stringify(result);
              }
            }
          } catch (exception) {
            console.log(exception);
            return express.response.sendStatus(500);
          }
        }
      );
    });

    app.listen(3000);

    console.log(
      "Express server has started on port 3000. Open http://localhost:3000/users to see results"
    );
  })
  .catch((error) => console.log(error));
